<?php
$dbuser = 'root';
$dbpass = 'root';
$mysql = new mysqli('localhost', $dbuser, $dbpass, 'mailing');
$mysql->query("SET NAMES utf8");
if(!$mysql) {
    die('Jest problem z połączenie do do bazy: '.$mysql->error);
}

function tabela() {
    global $mysql;
    $user_name = $_SESSION['user_name'];
    if(!$result = $mysql->query("SELECT * FROM `mail` ORDER BY `data` DESC")) {
        die("Problem z zapytaniem: ".$mysql->error);
    }
    echo "<table>";
    echo "<tr>";
    echo "<th id=\"naglowek\">Data</th>";
    echo "<th id=\"naglowek\">Godzina</th>";
    echo "<th id=\"naglowek\">Opiekun</th>";
    echo "<th id=\"naglowek\">Klient</th>";
    echo "<th id=\"naglowek\">Opis</a></th>";
    echo "<th id=\"naglowek\">Skasować ?</th>";
    echo "<th id=\"naglowek\">Edytuj ?</th>";
    echo "</tr>";
    while($row = $result->fetch_array(MYSQL_NUM)) {
        echo "<tr>";
        echo "<th>$row[1]</th>";
        echo "<th>$row[2]</th>";
        echo "<th>$row[3]</th>";
        echo "<th>$row[4]</th>";
        echo "<th>$row[5]</th>";
        echo "<th>";
        echo "<form action=\"\">";
        echo "<input type=\"checkbox\" name=\"usun\" value=\"$row[0]\">";
        echo "<input type=\"submit\" value=\"Tak\">";
        echo "</form>";
        echo "</th>";
        echo "<th>";
        echo "<form action=\"\">";
        echo "<input type=\"checkbox\" name=\"edytuj\" value=\"$row[0]\">";
        echo "<input type=\"submit\" value=\"Tak\">";
        echo "</form>";
        echo "</tr>";
    }
    echo "</table>";
    $usun = filter_input(INPUT_GET, 'usun');
    if(isset($usun)) {
        $mysql->query("DELETE FROM `mail` WHERE `id` = $usun AND `nazwisko` = '$user_name'" );
        echo '<script type="text/javascript">';
        echo 'location.href="index.php"';
        echo '</script>';
    }
    $edytuj = filter_input(INPUT_GET, 'edytuj');
    if(isset($edytuj) && $result = $mysql->query("SELECT * FROM `mail` WHERE `id` = $edytuj")) {
            $row = $result->fetch_array(MYSQL_NUM);
            echo "<form action=\"edytuj.php\" method=\"POST\">";
            echo "<table width=\"510\" border=\"0\" align=\"left\">";
            echo "<input type=\"hidden\" name=\"id\" value=\"$edytuj\">";
            echo "<tr>";
            echo "<td colspan=\"2\"></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Data: </td>";
            echo "<td><input type=\"text\" name=\"data\" id=\"datepicker\" value=\"$row[1]\" /></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Godzina: </td>";
            echo "<td><input type=\"text\" name=\"godzina\" id=\"godzina\" value=\"$row[2]\" /></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Opiekun: </td>";
            echo "<td><b>".$_SESSION['user_name']."</b></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Klient: </td>";
            echo "<td><input type=\"text\" name=\"klient\" id=\"klient\" value=\"$row[4]\" /></td>";
            echo "</tr>";
            echo "<tr>";
            echo "<td>Opis: </td>";
            echo "<td><textarea name=\"opis\" cols=\"50\" rows=\"10\">$row[5]</textarea></td>";
            echo "<tr>";
            echo "<td>&nbsp;</td>";
            echo "<td><input type=\"submit\" name=\"button\" id=\"button\" value=\"Edytuj\" /></td>";
            echo "</tr>";
            echo "</table>";
            echo "</form>";
    }
}
