<?php
/* Skrypt wykonany po to żeby nie zapisywać, do kogo był wysłany mailing, na kartce 
 * Wykonawca: Mateusz Mielczarek
 * Data: 11-03-2014
 * comperia.pl
 */ 
session_start();
if($_SESSION['user'] == 0) {
    header('Location: ../index.php');
}
include_once("../mysql.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"> 
        <link type="text/css" rel="stylesheet" href="../mail.css" />
        <title>Mailing 0.2.3</title>
    </head>
<body>
<?php
echo "<h2>Witaj ".$_SESSION['user_name']."</h2><br/>";
echo "<a href=\"index.php\">Strona głowna</a> | <a href=\"index.php?s=dodaj\">Dodaj mailing</a> | <a href=\"logout.php\">Wyloguj</a>";
tabela();
    if(isset($_GET['s'])) {
        switch($_GET['s']) {
            case "dodaj":
                include_once('tabela.php');
                break;
            case "usun":
                usun();
                break;
        }
    } 
$mysql->close();
?>
</body>
</html>