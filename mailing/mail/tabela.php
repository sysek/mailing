<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8">
        <title>Tabela</title>
        <link rel="stylesheet" href="../jquery-ui.css">
        <script src="../skrypty/jquery-1.10.2.js"></script>
        <script src="../skrypty/jquery-ui.js"></script>
        <link rel="stylesheet" href="../mail.css">
        <script>
            $(function() {
                $("#datepicker").datepicker({ dateFormat: "dd/mm/yy"});
            });
        </script>
    </head>
    <body>
        <br />
        <form id="form1" name="form1" method="post" action="dodaj.php">
            <table width="510" border="0" align="left">
                <tr>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>Data: </td>
                    <td><input type="text" name="data" id="datepicker" /></td>
                </tr>
                <tr>
                    <td>Godzina: </td>
                    <td><input type="text" name="godzina" id="godzina" /></td>
                </tr>
                <tr>
                    <td>Opiekun: </td>
                    <td><b><?php echo $_SESSION['user_name']?></b></td>
                </tr>
                <tr>
                    <td>Klient: </td>
                    <td><input type="text" name="klient" id="klient" /></td>
                </tr>
                <tr>
                    <td>Opis: </td>
                    <td><textarea name="opis" cols="50" rows="10"></textarea></td>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" name="button" id="button" value="Dodaj" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>